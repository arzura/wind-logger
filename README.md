VAISALA WMT52 Data Logger
=========================

ruby: jruby
platform: windows 7 pro 64bit

    bundle install
    bundle exec jruby dl.rb COM1

or

    run COM1

COM1 is serial port name

library:

* serial port gem
  - https://github.com/pmukerji/jruby-serialport
  - add Gemfile
    gem "jruby-serialport", :git => "git://github.com/pmukerji/jruby-serialport.git"
* RXTX for Java
  - http://fizzed.com/oss/rxtx-for-java
  - download jar file and dll

reference:

* Interrupt Handler: Ctrl+c to escape program
  - https://gist.github.com/hiremaga/320008
