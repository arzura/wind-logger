# VAISALA WMT52 Data Looger

require "serialport"
require 'csv'

class Handler
  def initialize serial_port
    @sp = serial_port
  end

  def gets
    message = ""
    loop do
      c = @sp.read(1)
      b = c.bytes.to_a[0]
      break if b == 13
      unless b == 0
        print c
        message << c
      end
    end
    message
  end

  def process message
    data = {}
    data[:time] = "#{Time.now.strftime '%Y-%m-%d %H:%M:%S'}"
    puts "#{data[:time]}"
    message.split(',').each do |item|
      puts "item:#{item}"
      if item.index('=')
        k, v = item[0...-1].split('=')
        x = item[-1]
        puts "data:#{k}>#{v}>#{x}"
        data[k.to_sym] = v.to_f
        data["#{k}x".to_sym] = x
      else
        data[:type] = item
      end
    end
    data
  end
end

puts "VAISALA WMT52 Data Logger"
# sp = SerialPort.new "CNCA0", 9600
puts "Initlaize serial port"
# sp = SerialPort.new "COM2", 9600
if ARGV.length < 1
  puts "Which serial port(eg. COM1, COM2, ...)?"
  return
end
sp = SerialPort.new(ARGV.first || "COM1", 19200)
unless sp
  puts "Check the connection."
  return
end
log = File.open("logs/test-#{Time.now.strftime '%Y%m%d%H%M%S'}.log", "w")
# log = File.open("test.log", "w+")
csv_file = CSV.open("out/data-#{Time.now.strftime '%Y%m%d%H%M%S'}.csv", "w", write_headers: true, headers: [:time, :Dn, :Dnx, :Dm, :Dmx, :Dx, :Dxx, :Sn, :Snx, :Sm, :Smx, :Sx, :Sxx])
puts "Start"
trap("SIGINT") {
  puts "Interrupt:sigint"
  Thread.list.each{ |thread| thread.exit unless thread == Thread.current }
  raise Interrupt
}
begin
  sp.read_timeout = 1000
  sp.write "Hello #{Time.now}\n"
  sleep 0.5
  Thread.new {
    puts "read terminal"
    handler = Handler.new sp
    puts "readline"
    loop do
      aline = handler.gets
      puts "[=>#{aline}]"
      log.print aline
      data = handler.process(aline.strip)
      csv_file << data if data[:type] == '0R1'
    end
  }
  loop do
    message = STDIN.gets
    sp.write message
    break if message.strip == "bye"
  end
  Thread.list.each{ |thread| thread.exit unless thread == Thread.current }
  sp.close if sp
  sp = nil;
  log.close if log
  log = nil;
  csv_file.close if csv_file
  csv_file = nil;
  puts "bye"
ensure
  puts "close serial port and log file"
  sp.close if sp
  log.close if log
  csv_file.close if csv_file
  puts "end threads"
  Thread.list.each{ |thread| thread.exit unless thread == Thread.current }
  puts "exit"
end
